'''a
Created on Jun 7, 2013

@author: bondaria

1 = LL - low learners class
2 = HL - High Learners class

HINT: most of "IndexError: list is out of range" appear due to bad naming of files! be careful!
'''
#import settings for Weka classifiers
from WekaTools import *
import numpy as np
from stattools import *
import RandomMethods
USERS = weka_params.USERS
CLASSES = 2
NRUNS = 10
# read feature files and select features
def read_Arff_file(input):
    #read file
    f = open(input, 'r')
    lines = f.readlines()
    f.close
    print "Processing file " + input
    nstr = 0
    
    #read all attributes
    
    # features are located in dictionary
    # KEY: attribute name
    # VALUE:  [] - list of features from first participant to the last participant
    featdict = {}
    featheaders = []
    while lines[nstr]!="@data\n":
        if lines[nstr].count("\t") > 0:
            linar = lines[nstr][:-1].split("\t")
        #if lines[nstr].count(",") > 1:
        #    linar = lines[nstr][:-1].split(",")
        #if lines[nstr].count(" ") > 0:
        #    linar = lines[nstr][:-1].split(" ")
            if linar[0] == "@attribute":
                featdict[linar[1]] = []
                featheaders.append(linar[1])
        nstr+=1
    #pass all blank strings if any
    while len(lines[nstr]) < 8:
        nstr += 1
    print lines[nstr]    
    # go through all lines with feature values 
    for line in lines[nstr:]:
        linar = line[:-1].split(",")
        #print len(linar)
        #print len(featheaders)
        #print featheaders
        for i in xrange(len(linar)):
            featdict[featheaders[i]].append(linar[i])
        
    return featdict


def CalculateAccuracy(actualValue, predictedValue, USERS):
    TLL = 0
    FLL = 0
    THL = 0
    FHL = 0
    for i in xrange(USERS):
        #print i
        #print actualValue
        #print predictedValue
        if actualValue[i] == predictedValue[i]:
            if actualValue[i] == 1:
                TLL += 1
            else:
                THL += 1
        else:
            if actualValue[i] == 1:
                FHL += 1
            else:
                FLL += 1
    accuracy = (TLL+THL)*1.0/(TLL+THL+FLL+FHL)*100
    return accuracy



#Home
DIR = "C:\\Users\\Daria\\Dropbox\\MT_Ensemble\\input\\"
#Lab
DIR = "C:\\Users\\bondaria\\Dropbox\\MT_Ensemble\\input\\"
METHODS = ["ensemble_Simple_Logistic_Classify", "ensemble_Multilayer_Perceptron", "ensemble_Naive_Bayes"]

def RunEnsembleClassifier(METHOD, DIR, trainfile, tesfile, USERS):
    """
     this method run Ensemble Classification, "
     Methods -  a list of methods to run it on
     Input - a list of input files that correspond to Methods
    """
    resfiles = []
    dict_results = {}
    for i in xrange(len(METHODS)):
        #trainfile = INPUT[i][:-5]+"train.arff"
        #testfile = INPUT[i][:-5]+"test.arff"
        #resultfile = METHODS[i]+str(i) + "_result.txt"
        resultfile = testfile[:-5] + "_result.txt"
        resfiles.append(resultfile)
        #print METHODS[i]+"(DIR, trainfile, resultfile)"
        exec(METHODS+"(DIR,  trainfile, testfile, resultfile)")
            #resfiles.append(resultfile)
        
    dictModelResult = {}
    print resfiles
    # The following part of the code compares predictions of 3 models
    for file in resfiles:
        #res contains pairs of values: [<actual>, <predicted>] 
        dictModelResult [file] = []
        res=[]
        resfile=open(DIR + file) 
        for line in resfile.readlines():
            if line.count(":")==2:
                c = line.split(":")
                res.append([int(c[0][-1]),int(c[1][-1])])
                # [<actual>, <predicted>] 
                #print [int(c[0][-1]),int(c[1][-1])]
        dictModelResult[file] = res
        #print res
    #print dictModelResult
    
    resEnsemble = np.zeros([USERS, CLASSES]) #<2 lines correspond to two classes>
    actualValue = np.zeros([USERS])
    results = np.zeros([USERS])
    print "Number of users = ", str(USERS)
    #calculate actual value
    for i in xrange(USERS):
        actualValue[i] = dictModelResult[dictModelResult.keys()[0]][i][0]
    for key in dictModelResult.keys():
        dict_results[key] = np.zeros([USERS])
    for i in xrange(USERS):
        #keyModelResult - unique value for each feature set
        for keyModelResult in dictModelResult.keys():
            print "keyModelResult whatever it is: " + keyModelResult
            #check if actual values match between the models
            el = dictModelResult[keyModelResult][i]
            #print el
            if el[0] == actualValue[i]:
                #if matched
                #increment corresponding element of array
                #print i, el[1]
                resEnsemble[i, el[1]-1]+= 1
                dict_results[keyModelResult][i] = el[1]
            else: print "Error! Folds don't match!"
        #majority voting
        results[i] = np.argmax(resEnsemble[i])+1

    print "Ensemble Model Results"
    print "ActualValue ", actualValue
    print "Predicted Value ", results
#     print "accuracy:", CalculateAccuracy(actualValue, results, USERS)
#     TLL, FLL, THL, FHL = CalulcateStatsFromLists(actualValue, results)
#     print TLL, FLL, THL, FHL
#     acc_overall = accuracy(TLL+THL, FLL+FHL)
#     acc_LL = accuracy(TLL, FHL)
#     acc_HL =  accuracy(THL, FLL)
#     kap = kappa(TLL, FLL, THL, FHL) 
#     
#     print "Overall Accuracy: ", acc_overall
#     print "LL accuracy: ", acc_LL
#     print "HL accuracy: ", acc_HL
#     print "Kappa score: ", kap 
    
    #return resfiles, acc_overall, acc_LL, acc_HL, kap
    dict_results["actualResults"] = actualValue
    dict_results["Ensemble Model Results"] = results
    print dict_results
    return resfiles, actualValue, results, dict_results


USERS = weka_params.USERS
def Generate2Classes_Samples(INPUT, CLASSLABELS = ["LL", "HL"]):
    print "Generate2Classes_Samples(INPUT)"
    with open(INPUT, 'r') as f:
        lines = f.readlines()
        curstr = 0
    while (lines[curstr].find("@data")==-1):
        curstr += 1
    curstr += 1
    dict_classes = {}
    for el in CLASSLABELS:
        dict_classes[el] = []
    i = 0
    while curstr < len(lines):
        line = lines[curstr]
        dict_classes[line[-3:-1]].append(i)
        i += 1
        curstr += 1
    return dict_classes 
            
def CalculateStatsFromFile(DIR, inputfile, USERS):
    actual = []
    predicted = []
    resfile=open(DIR + inputfile) 
    for line in resfile.readlines():
        if line.count(":")==2:
            c = line.split(":")
            actual.append(int(c[0][-1]))
            predicted.append(int(c[1][-1]))
            #print [int(c[0][-1]),int(c[1][-1])]
    #print actual
    #print predicted
    return CalulcateStatsFromLists(actual, predicted, USERS)
    
def CalulcateStatsFromLists(actual, predicted, USERS):
    TLL = 0
    FLL = 0
    THL = 0
    FHL = 0
    for i in xrange(USERS):
        #print i
        #print actualValue
        #print predictedValue
        #print actual[i], predicted[i]
        if actual[i] == predicted[i]:
            if actual[i] == 1:
                TLL += 1
            elif actual[i] == 2:
                THL += 1
            else:
                print "ERROR!!!!"
        else:
            if actual[i] == 1:
                FHL += 1
            elif actual[i] == 2:
                FLL += 1
            else:
                print "Error!!!"
        
    return TLL, FLL, THL, FHL


METHODS = [["Simple_Logistic_Regression_RepCV", "Simple_Logistic_Regression_RepCV", "Simple_Logistic_Regression_RepCV"],
           ["Multilayer_Perceptron_RepCV", "Multilayer_Perceptron_RepCV", "Multilayer_Perceptron_RepCV"],
           ["Naive_Bayes_RepCV", "Naive_Bayes_RepCV", "Naive_Bayes_RepCV"],
           ["Random_Forest_RepCV","Random_Forest_RepCV", "Random_Forest_RepCV"],
           ["MultinomialLogisticRegression_RepCV", "MultinomialLogisticRegression_RepCV", "MultinomialLogisticRegression_RepCV"]
          ]
INPUT = [["Actions_30f_Best1st_SimpleLogistic_10f.arff","GazeWrapper_best1st_SimpleLogistic_10f.arff","FullWrapper_Best1st_SimpleLogistic_10f.arff"],
         ["Actions_30f_Best1st_MultilayerPerc_10f.arff", "GazeWrapper_Best1st_MultilayerPerc_10f.arff", "FullWrapper_Best1st_MultilayerPercpetron_10f.arff"],
         ["Actions_30f_Best1st_Naive_10f.arff", "GazeWrapper_best1st_NaiveBayes_11f.arff", "FullWrapper_Best1st_NaiveBayes_10f.arff"],
         ["Actions_30f_Best1st_RandomForest_15f.arff", "Gaze_Best1st_Random_12f.arff", "FullWrapper_Best1st_RandomForest_10f.arff"],
         ["Actions_30f_Best1st_Logistic_10f.arff", "Gaze_Best1st_Logistic_10percent.arff", "FullWrapper_Best1st_Logistic_10f.arff"]
         ]
SUBDIR = ["SimpleLogisticRegression", "MultilayerPerceptron", "NaiveBayes", "RandomForest", "MultinomialLogisticRegression"]
methodsnum = 1 #len(METHODS)
lines = []
lines.append("METHOD Name\tOverall Acc\tLL Accuracy \t HL accuracy \t kappa\n")
# Run Ensemble Model for each Algorithm
for i in xrange(methodsnum):
    resfiles = []
    resfiles, actualValues, predictedValues = RunEnsembleClassifier(METHODS[i], DIR+SUBDIR[i]+"\\", INPUT[i], USERS, 50)
    lines.append(METHODS[i][0]+"\n")
    #lines.append("ENSEMBLE\t"+str(en_acc_overall) +"\t" +str(en_acc_LL) + "\t" +str(en_acc_HL) + "\t" + str(en_kap)+"\n")
    
    
    
    
    for METHOD in resfiles:
        #print METHOD
        TLL, FLL, THL, FHL = CalculateStatsFromFile(DIR+SUBDIR[i]+"\\", METHOD, USERS)
        print TLL, FLL, THL, FHL
        acc_overall = accuracy(TLL+THL, FLL+FHL)
        acc_LL = accuracy(TLL, FHL)
        acc_HL =  accuracy(THL, FLL)
        kap = kappa(TLL, FLL, THL, FHL) 
        lines.append(METHOD + "\t"+str(acc_overall) +"\t" +str(acc_LL) + "\t" +str(acc_HL) + "\t" + str(kap)+"\n")
        #print "Overall Accuracy: ", acc_overall
        #print "LL accuracy: ", acc_LL
        #print "HL accuracy: ", acc_HL
        #print "Kappa score: ", kap 
input = DIR + "ResultModeling.tsv"
with open(input, 'w') as f:
    for line in lines:
        #print line
        f.write(line)

DIR = "C:\\Users\\bondaria\\Dropbox\\MT_Ensemble\\input\\MultinomialLogisticRegression\\"
dictClasses =  Generate2Classes_Samples(DIR+"Actions_30f_Best1st_Logistic_10f.arff")

runfolds =  RandomMethods.GenerateFolds_for_nruns(dictClasses["LL"], dictClasses["HL"], RandomMethods.distr, 10,10)
#for Run in runfolds:
def RunEnsembleRepeatedCV(METHOD, DIR)
Run = runfolds[0]
nfold = 0
for testfold in Run:
    INPUT =  DIR+"Actions_30f_Best1st_Logistic_10f.arff"
    trainfile = open(INPUT[:-5]+"-train"+str(nfold)+".arff","w")
    testfile = open(INPUT[:-5]+"-test"+str(nfold)+".arff","w")
    with open(INPUT, 'r') as f:
        lines = f.readlines()
        curstr = 0
        while (lines[curstr].find("@data") == -1):
            trainfile.write(lines[curstr])
            testfile.write(lines[curstr])
            curstr += 1
        
        trainfile.write(lines[curstr])
        testfile.write(lines[curstr])
        curstr += 1
        i = 0
        while curstr < len(lines):
            if i in testfold:
                testfile.write(lines[curstr])
            else:
                trainfile.write(lines[curstr])
            i += 1
            curstr += 1
    testfile.close()
    trainfile.close()
    nfold+=1
#10 runs code
#METHODS = ["ensemble_Simple_Logistic_Classify"]
#trainfile = "GazeWrapper_best1st_Simple Logistic_10f.arff"
#resultfile = "testres"
## process 10 run 10 fold
#n_accur_overall = []
#n_accur_LL = []
#n_accur_HL = []
#n_kap = []
#n_TLL = 0
#n_FLL = 0
#n_THL = 0
#n_FHL = 0
#resfiles = []
#for i in xrange(NRUNS):
#    exec("ensemble_Simple_Logistic_Classify(DIR,  trainfile, '"+ resultfile +str(i) +".txt', 10, i)")
#    resfiles.append(resultfile +str(i) +".txt")




#for input in resfiles:
#    TLL, FLL, THL, FHL = CalculateStatsFromFile(DIR, input, USERS)
#    #print TLL, FLL, THL, FHL
#    n_TLL += TLL
#    n_FLL += FLL
#    n_THL += THL
#    n_FHL += FHL
#    accur_overall = accuracy(TLL+THL, FLL + FHL)
#    accur_LL = accuracy(TLL, FHL)
#    accur_HL = accuracy(THL, FLL)
#    kap = kappa(TLL, FLL, THL, FHL)  
#    #print accur_overall, accur_LL, accur_HL, kap
#    n_accur_overall.append(accur_overall)
#    n_accur_LL.append(accur_LL)
#    n_accur_HL.append(accur_HL)
#    n_kap.append(kap)
##calculate stats for the model
#final_accur_overall = 1.0 * reduce(lambda x, y: x + y, n_accur_overall) / len(n_accur_overall)
#final_accur_LL = 1.0 * reduce(lambda x, y: x + y, n_accur_LL) / len(n_accur_overall)
#final_accur_HL = 1.0 * reduce(lambda x, y: x + y, n_accur_HL) / len(n_accur_overall)
#final_kappa = 1.0 * reduce(lambda x, y: x + y, n_kap) / len(n_accur_overall)
#print final_accur_overall, final_accur_LL, final_accur_HL, final_kappa
    
