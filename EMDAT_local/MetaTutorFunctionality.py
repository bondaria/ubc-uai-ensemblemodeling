'''
Created on Dec 10, 2012
This file is created to fix a PARTICULAR problem of merging: 
NormalImageContent and ImageContentRestrictedInput to one AOI: ImageContent
NormalTextContent and TextContentRestrictedInput to one AOI: TextContent
@author: Daria
'''
from AOI import  *
from copy import deepcopy
def FixAOIs(scene):
    print "Performing additional calculations to fix changing layouts"
    MergeToXContent (scene, "TextContent", "NormalTextContent", "TextContentRestrictedInput")
    MergeToXContent (scene, "ImageContent", "NormalImageContent", "ImageContentRestrictedInput")
    FixFromForAllAOIs(scene)
    FullViewFix(scene)
    return

#Merge AOIs NormalImageContent and ImageContentRestrictedInput to ImdageContent 
def MergeToXContent(scene, newname, normalaoi, restrictedaoi):
    print "Merging "+ normalaoi + " and " + restrictedaoi
    #print "Normal aoi: " + str(scene.aoi_data[normalaoi])
    #print "Restricted AOI: " + scene.aoi_data[restrictedaoi]
    if (scene.aoi_data.has_key(normalaoi)) and (scene.aoi_data.has_key(restrictedaoi)): #if both AOIs are not empty
        MergeToXContent_noNone (scene, newname, normalaoi, restrictedaoi) 
    else:
        if (scene.aoi_data.has_key(normalaoi)): #if restricted aoi is empty
            aoidata = deepcopy(scene.aoi_data[normalaoi])
            aoidata.aoi.aid = newname
            scene.aoi_data[newname]=aoidata
        if (scene.aoi_data.has_key(restrictedaoi)): #if normal AOI is empty
            aoidata = deepcopy(scene.aoi_data[restrictedaoi])
            aoidata.aoi.aid = newname
            scene.aoi_data[newname]=aoidata

def MergeToXContent_noNone (scene, newname, normalaoi, restrictedaoi):
    
    aoi = deepcopy(scene.aoi_data[normalaoi].aoi)
    aoi.aid = newname
    #def __init__(self,aoi,seg_fixation_data, starttime, endtime, active_aois):
    fnorm = scene.aoi_data[normalaoi].features
    frest = scene.aoi_data[restrictedaoi].features
    aoistat= AOI_Stat(aoi, [], 0, 1,[])
    #create new feature set
    aoistat.features={}
    aoistat.features['numfixations'] =fnorm['numfixations']+frest['numfixations']
    aoistat.features['longestfixation'] = max(fnorm['longestfixation'],frest['longestfixation'])
    aoistat.features['timetofirstfixation'] = -1
    aoistat.features['timetolastfixation'] = -1
    # Natasha's fix
#     if aoistat.features['totaltimespent'] != 0:
#         aoistat.features['fixationrate'] = (float(aoistat.features['numfixations'])) / aoistat.features['totaltimespent']
#     else:
#         aoistat.features['fixationrate'] = 0
        
    aoistat.features['fixationsonscreen']=fnorm['fixationsonscreen']+frest['fixationsonscreen']
    aoistat.features['timeonscreen'] = fnorm['timeonscreen']+frest['timeonscreen']
    
    aoistat.features['totaltimespent'] =  fnorm['totaltimespent']+frest['totaltimespent']
            
    aoistat.features['proportionnum_dynamic'] = (float(aoistat.features['numfixations'])) / aoistat.features['fixationsonscreen']
    aoistat.features['proportiontime_dynamic'] = (float(aoistat.features['totaltimespent'])) / aoistat.features['timeonscreen']
    aoistat.features['proportiontime'] = fnorm['proportiontime']+frest['proportiontime']
    aoistat.features['proportionnum'] = fnorm['proportionnum']+frest['proportionnum']          
    aoistat.features['fixationrate'] = (float(aoistat.features['numfixations'])) / aoistat.features['totaltimespent']
    
    aoistat.features['sumtransfrom']=fnorm['sumtransfrom']+frest['sumtransfrom']
    aoistat.features['sumtransto']=fnorm['sumtransto']+frest['sumtransto']
    #print "!!!!!!!!!!!!!"
    #print scene.aoi_data.keys()    
    for elaoi in scene.aoi_data.keys():
        if elaoi is not "TextContent" and elaoi is not "ImageContent":
            aid = elaoi
            #print "currently in: " + elaoi
            
            if fnorm.has_key('numtransto_%s'%(aid)):
                if frest.has_key('numtransto_%s'%(aid)):
                    aoistat.features['numtransto_%s'%(aid)] = fnorm['numtransto_%s'%(aid)]+frest['numtransto_%s'%(aid)]
                    aoistat.features['numtransfrom_%s'%(aid)] = fnorm['numtransfrom_%s'%(aid)]+frest['numtransfrom_%s'%(aid)]
                    if aoistat.features['sumtransto']!=0:
                        aoistat.features['proptransto_%s'%(aid)] = (float(aoistat.features['numtransto_%s'%(aid)]))/  aoistat.features['sumtransto']
                    else:
                        aoistat.features['proptransto_%s'%(aid)] = 0
                    if aoistat.features['sumtransfrom']!=0:
                        aoistat.features['proptransfrom_%s'%(aid)] = (float(aoistat.features['numtransfrom_%s'%(aid)]))/  aoistat.features['sumtransfrom']
                    else:
                        aoistat.features['proptransfrom_%s'%(aid)] = 0
                else:
                    aoistat.features['numtransto_%s'%(aid)] = fnorm['numtransto_%s'%(aid)]
                    aoistat.features['numtransfrom_%s'%(aid)] = fnorm['numtransfrom_%s'%(aid)]
                    if aoistat.features['sumtransto']!=0:
                        aoistat.features['proptransto_%s'%(aid)] = (float( aoistat.features['numtransto_%s'%(aid)])) / aoistat.features['sumtransto']
                    else:
                        aoistat.features['proptransto_%s'%(aid)] = 0
                    if aoistat.features['sumtransfrom']!=0:
                        aoistat.features['proptransfrom_%s'%(aid)] =  (float( aoistat.features['numtransfrom_%s'%(aid)])) / aoistat.features['sumtransfrom']
                    else:
                        aoistat.features['proptransfrom_%s'%(aid)] = 0
            else:
                if frest.has_key('numtransto_%s'%(aid)):
                    aoistat.features['numtransto_%s'%(aid)] = frest['numtransto_%s'%(aid)]
                    aoistat.features['numtransfrom_%s'%(aid)] = frest['numtransfrom_%s'%(aid)]
                    if aoistat.features['sumtransto']!=0:
                        aoistat.features['proptransto_%s'%(aid)] = (float( aoistat.features['numtransto_%s'%(aid)])) / aoistat.features['sumtransto']
                    else:
                        aoistat.features['proptransto_%s'%(aid)] = 0
                    if aoistat.features['sumtransfrom']!=0:
                        aoistat.features['proptransfrom_%s'%(aid)] =  (float( aoistat.features['numtransfrom_%s'%(aid)])) / aoistat.features['sumtransfrom']
                    else:
                        aoistat.features['proptransfrom_%s'%(aid)]  = 0

    
    scene.aoi_data[newname] = aoistat

def FixFromForAllAOIs(scene):
    print "Fixing calculations of number of transitions"
    for aoiname in scene.aoi_data.keys():
        #print aoiname
        #fix transitions related to TextContent area
        if scene.aoi_data[aoiname].features.has_key("numtransto_NormalTextContent"):
            if scene.aoi_data[aoiname].features.has_key("numtransto_TextContentRestrictedInput"):
                
                scene.aoi_data[aoiname].features["numtransto_TextContent"]=scene.aoi_data[aoiname].features["numtransto_TextContentRestrictedInput"]+scene.aoi_data[aoiname].features["numtransto_NormalTextContent"]
                scene.aoi_data[aoiname].features["numtransfrom_TextContent"]=scene.aoi_data[aoiname].features["numtransfrom_TextContentRestrictedInput"]+scene.aoi_data[aoiname].features["numtransfrom_NormalTextContent"]
                scene.aoi_data[aoiname].features["proptransto_TextContent"]=scene.aoi_data[aoiname].features["proptransto_TextContentRestrictedInput"]+scene.aoi_data[aoiname].features["proptransto_NormalTextContent"]
                scene.aoi_data[aoiname].features["proptransfrom_TextContent"]=scene.aoi_data[aoiname].features["proptransfrom_TextContentRestrictedInput"]+scene.aoi_data[aoiname].features["proptransfrom_NormalTextContent"]
            else:
                scene.aoi_data[aoiname].features["numtransto_TextContent"]=scene.aoi_data[aoiname].features["numtransto_NormalTextContent"]
                scene.aoi_data[aoiname].features["numtransfrom_TextContent"]=scene.aoi_data[aoiname].features["numtransfrom_NormalTextContent"]
                scene.aoi_data[aoiname].features["proptransto_TextContent"]=scene.aoi_data[aoiname].features["proptransto_NormalTextContent"]
                scene.aoi_data[aoiname].features["proptransfrom_TextContent"]=scene.aoi_data[aoiname].features["proptransfrom_NormalTextContent"]
        else:
            if scene.aoi_data[aoiname].features.has_key("numtransto_TextContentRestrictedInput"):
                scene.aoi_data[aoiname].features["numtransto_TextContent"]=scene.aoi_data[aoiname].features["numtransto_TextContentRestrictedInput"]
                scene.aoi_data[aoiname].features["numtransfrom_TextContent"]=scene.aoi_data[aoiname].features["numtransfrom_TextContentRestrictedInput"]
                scene.aoi_data[aoiname].features["proptransto_TextContent"]=scene.aoi_data[aoiname].features["proptransto_TextContentRestrictedInput"]
                scene.aoi_data[aoiname].features["proptransfrom_TextContent"]=scene.aoi_data[aoiname].features["proptransfrom_TextContentRestrictedInput"]
                
        #fix transitions related to ImageContent area
        if scene.aoi_data[aoiname].features.has_key("numtransto_NormalImageContent"):
            if scene.aoi_data[aoiname].features.has_key("numtransto_ImageContentRestrictedInput"):
                
                scene.aoi_data[aoiname].features["numtransto_ImageContent"]=scene.aoi_data[aoiname].features["numtransto_ImageContentRestrictedInput"]+scene.aoi_data[aoiname].features["numtransto_NormalImageContent"]
                scene.aoi_data[aoiname].features["numtransfrom_ImageContent"]=scene.aoi_data[aoiname].features["numtransfrom_ImageContentRestrictedInput"]+scene.aoi_data[aoiname].features["numtransfrom_NormalImageContent"]
                scene.aoi_data[aoiname].features["proptransto_ImageContent"]=scene.aoi_data[aoiname].features["proptransto_ImageContentRestrictedInput"]+scene.aoi_data[aoiname].features["proptransto_NormalImageContent"]
                scene.aoi_data[aoiname].features["proptransfrom_ImageContent"]=scene.aoi_data[aoiname].features["proptransfrom_ImageContentRestrictedInput"]+scene.aoi_data[aoiname].features["proptransfrom_NormalImageContent"]
            else:
                scene.aoi_data[aoiname].features["numtransto_ImageContent"]=scene.aoi_data[aoiname].features["numtransto_NormalImageContent"]
                scene.aoi_data[aoiname].features["numtransfrom_ImageContent"]=scene.aoi_data[aoiname].features["numtransfrom_NormalImageContent"]
                scene.aoi_data[aoiname].features["proptransto_ImageContent"]=scene.aoi_data[aoiname].features["proptransto_NormalImageContent"]
                scene.aoi_data[aoiname].features["proptransfrom_ImageContent"]=scene.aoi_data[aoiname].features["proptransfrom_NormalImageContent"]
        else:
            if scene.aoi_data[aoiname].features.has_key("numtransto_ImageContentRestrictedInput"):
                scene.aoi_data[aoiname].features["numtransto_ImageContent"]=scene.aoi_data[aoiname].features["numtransto_ImageContentRestrictedInput"]
                scene.aoi_data[aoiname].features["numtransfrom_ImageContent"]=scene.aoi_data[aoiname].features["numtransfrom_ImageContentRestrictedInput"]
                scene.aoi_data[aoiname].features["proptransto_ImageContent"]=scene.aoi_data[aoiname].features["proptransto_ImageContentRestrictedInput"]
                scene.aoi_data[aoiname].features["proptransfrom_ImageContent"]=scene.aoi_data[aoiname].features["proptransfrom_ImageContentRestrictedInput"]
                
def FullViewFix(scene):# combines TextContent and ImageContent with FullView
    aois_list = [["ExpandedTextContent", "TextContent"], ["ExpandedImageContent","ImageContent"]] 
    print "Fixing FullView"
    for aoi_name in aois_list:
        print aoi_name
        if scene.aoi_data.has_key(aoi_name[0]):
            print scene.aoi_data[aoi_name[1]].features
            scene.aoi_data[aoi_name[1]].features['numfixations'] += scene.aoi_data[aoi_name[0]].features['numfixations']
            print "previous value: " + str(scene.aoi_data[aoi_name[1]].features['longestfixation'])
            print "new value: " + str(max(scene.aoi_data[aoi_name[1]].features['longestfixation'],scene.aoi_data[aoi_name[0]].features['longestfixation']))
            
            scene.aoi_data[aoi_name[1]].features['longestfixation'] = max(scene.aoi_data[aoi_name[1]].features['longestfixation'],scene.aoi_data[aoi_name[0]].features['longestfixation'])
            scene.aoi_data[aoi_name[1]].features['totaltimespent'] +=  scene.aoi_data[aoi_name[0]].features['totaltimespent']
            print "previous value: " + str(scene.aoi_data[aoi_name[1]].features['fixationrate'])
            scene.aoi_data[aoi_name[1]].features['fixationrate'] = (float(scene.aoi_data[aoi_name[1]].features['numfixations'])) / scene.aoi_data[aoi_name[1]].features['totaltimespent']
            print "new value: " + str(scene.aoi_data[aoi_name[1]].features['fixationrate'])
            
        else:
            print aoi_name[0], "was not found in dictionary of features"