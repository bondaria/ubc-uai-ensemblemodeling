'''
Created on Aug 2, 2013

@author: bondaria
'''
NRUNS = 10#number of run in repeated CV
NFOLDS = 10
from runEnsemble_repeatedCV import *
from datetime import datetime
NFEATURESETS = 3
SUFFIX = "_seed_8_to_thesis"

#ubc
DIR = "C:\\Users\\bondaria\\Documents\\UAI\\temp\\input\\"
OUTDIR = "C:\\Users\\bondaria\\Documents\\UAI\\temp\\output_final\\"
#home
#DIR = "D:\\Studies\\UAI\\temp\\input\\"

Classifiers = {"Simple_Logistic_Regression_RepCV" : ["Actions36_SimpleLogistic_11.arff","GazeWrapper_best1st_SimpleLogistic_10f.arff","Backward_Full110_SimpleLogistic_10f.arff"],
                   "Multilayer_Perceptron_RepCV" : ["Actions36_MultilayerPerceptron_10.arff", "GazeWrapper_Best1st_MultilayerPerc_10f.arff", "Full110_MultilayerPerceptron_10.arff"],
                   "Naive_Bayes_RepCV" : ["Actions36_NaiveBayes.arff", "GazeWrapper_best1st_NaiveBayes_10f.arff", "Full110_NaiveBayes_10arff.arff"],
                   "Random_Forest_RepCV" : ["Actions36_RandomForest_12.arff", "Gaze_Best1st_Random_12f.arff", "Full110_RandomForest_10arff.arff"],
                   "MultinomialLogisticRegression_RepCV" : ["Actions36_Logistic.arff", "Gaze_Best1st_Logistic_10percent.arff", "Full110_MultinomialLogistic_10.arff"]
                   }
LOG = "LOG_"+ SUFFIX + ".txt"
log = open(OUTDIR+LOG, "w")
METHODS = ["Simple_Logistic_Regression_RepCV", "MultinomialLogisticRegression_RepCV", "Naive_Bayes_RepCV", "Random_Forest_RepCV", "Multilayer_Perceptron_RepCV"] #Classifiers.keys()
lines = []
line = "METHOD Name\tFile\tOverall Acc\tVariance\tLL Accuracy \t HL accuracy \t kappa\t"
for i in xrange(NRUNS):
    line = line + "acc_run_" + str(i+1)+"\t"
for i in xrange(NRUNS):
    line = line + "kappa_run_" + str(i+1)+"\t"
for i in xrange(NRUNS):
    line = line + "LL_acc_run_" + str(i+1)+"\t"
for i in xrange(NRUNS):
    line = line + "HL_acc_run_" + str(i+1)+"\t"
lines.append(line[:-1]+"\n")


""" Create folds for stratified 10 run 10 fold cross validation"""

INPUT = Classifiers[METHODS[0]][0]
CVSamples = CreateSamplesStratified(DIR+INPUT, N = NRUNS, K = NFOLDS) #create folds for 10 run 10 fold cross validation
print CVSamples
#create folds for Leave One Out Cross Validation
# CVSamples = []
# el = []
# for i in xrange(50):
#     el.append([i])
# CVSamples.append(el)
""" basic classifiers """
for i in xrange(NFEATURESETS):
    for METHOD in METHODS:
        print "Working on Classifier "+ METHOD
        log.write("METHOD: " +  METHOD)
        INPUT = Classifiers[METHOD][i]
        dict_Classifiers = {}
        dict_Classifiers[METHOD] = [INPUT]
        start = datetime.now()
        final_acc, final_var, final_LL_acc, final_HL_acc, final_kappa, run_acc, run_LL_acc, run_HL_acc, run_kappa = runClassifierWithRepeatedCV(DIR, dict_Classifiers, NRUNS, NFOLDS, CVSamples)
        end = datetime.now()
        line = METHOD + "\t"  + INPUT + "\t"+str(final_acc) +"\t"+ str(final_var*100) +"\t" +str(final_LL_acc) + "\t" +str(final_HL_acc) + "\t" + str(final_kappa)+"\t"
        line = line + (str(run_acc)[1:-1]).replace(",", "\t") +"\t" + (str(run_kappa)[1:-1]).replace(",", "\t") + "\t" + (str(run_LL_acc)[1:-1]).replace(",", "\t") +  "\t" + (str(run_HL_acc)[1:-1]).replace(",", "\t") + "\n"
        diff = end - start
        duration = str(diff.seconds)+str(".")+str(diff.microseconds / 1000)
        print "INPUT: "+INPUT+". \nCalculations took: " + duration
        log.write("INPUT: "+INPUT+". \nCalculations took: " + duration)
        lines.append(line)
""" ensemble modeling """

print "calculating ensemble ... "
log.write("Ensemble modeling")
for METHOD in METHODS:
    dict_Classifiers={}
    dict_Classifiers[METHOD] = Classifiers[METHOD]
    start = datetime.now()
    final_acc, final_var, final_LL_acc, final_HL_acc, final_kappa, run_acc, run_LL_acc, run_HL_acc, run_kappa = runClassifierWithRepeatedCV(DIR, dict_Classifiers, NRUNS, NFOLDS, CVSamples, True)
    end = datetime.now()
    diff = end - start
    duration = str(diff.seconds)+str(".")+str(diff.microseconds / 1000)
    print "Method: "+METHOD+". \nCalculations took " + duration + "\n"
    log.write("Method: "+METHOD+". \nCalculations took " + duration)
    line = METHOD + "\t"  + "EnsembleMajority" + "\t"+str(final_acc) +"\t"+ str(final_var*100) +"\t" +str(final_LL_acc) + "\t" +str(final_HL_acc) + "\t" + str(final_kappa)+"\t"
    line = line + (str(run_acc)[1:-1]).replace(",", "\t") +"\t" + (str(run_kappa)[1:-1]).replace(",", "\t") + "\t" + (str(run_LL_acc)[1:-1]).replace(",", "\t") +  "\t" + (str(run_HL_acc)[1:-1]).replace(",", "\t") + "\n"
    lines.append(line)    
# 
# print "calculating ensemble with summation distribution... "
# for METHOD in METHODS[:]:
#     dict_Classifiers={}
#     dict_Classifiers[METHOD] = Classifiers[METHOD]
#     final_acc, final_var, final_LL_acc, final_HL_acc, final_kappa, run_acc, run_LL_acc, run_HL_acc, run_kappa = runClassifierWithRepeatedCV(DIR, dict_Classifiers, NRUNS, NFOLDS, CVSamples, True, "Distr")
#     line = METHOD + "\t"  + "EnsembleDistr" + "\t"+str(final_acc) +"\t"+ str(final_var*100) +"\t" +str(final_LL_acc) + "\t" +str(final_HL_acc) + "\t" + str(final_kappa)+"\t"
#     line = line + (str(run_acc)[1:-1]).replace(",", "\t") +"\t" + (str(run_kappa)[1:-1]).replace(",", "\t") + "\t" + (str(run_LL_acc)[1:-1]).replace(",", "\t") +  "\t" + (str(run_HL_acc)[1:-1]).replace(",", "\t") + "\n"
#     lines.append(line)
input = OUTDIR + "EnsembleResult"+ SUFFIX +".tsv"
with open(input, 'w') as f:
    for line in lines:
        #print line
        f.write(line)

""" Ensemble Modeling with different combinations """ 
# 
# print "working on Actions"
# Classifiers_Actions = {"Simple_Logistic_Regression_RepCV" : ["Actions36_SimpleLogistic_11.arff"],
#                    "Multilayer_Perceptron_RepCV" : ["Actions36_MultilayerPerceptron_10.arff"],
#                    "Naive_Bayes_RepCV" : ["Actions36_NaiveBayes.arff"],
#                    #"Random_Forest_RepCV" : ["Actions36_RandomForest_12.arff"],
#                    #"MultinomialLogisticRegression_RepCV" : ["Actions36_Logistic.arff"]
#                    }
# final_acc, final_var, final_LL_acc, final_HL_acc, final_kappa, run_acc, run_LL_acc, run_HL_acc, run_kappa = runClassifierWithRepeatedCV(DIR, Classifiers_Actions, NRUNS, NFOLDS, CVSamples, True)
# print final_acc, final_var, final_LL_acc, final_HL_acc, final_kappa, run_acc, run_LL_acc, run_HL_acc, run_kappa
# line = "Actions" + "\t"  + "EnsembleActions" + "\t"+str(final_acc) +"\t"+ str(final_var*100) +"\t" +str(final_LL_acc) + "\t" +str(final_HL_acc) + "\t" + str(final_kappa)+"\t"
# line = line + (str(run_acc)[1:-1]).replace(",", "\t") +"\t" + (str(run_kappa)[1:-1]).replace(",", "\t") + "\t" + (str(run_LL_acc)[1:-1]).replace(",", "\t") +  "\t" + (str(run_HL_acc)[1:-1]).replace(",", "\t") + "\n"
# lines.append(line)
#  
# print "working on Gaze"
# Classifiers_Gaze = {"Simple_Logistic_Regression_RepCV" : ["GazeWrapper_best1st_SimpleLogistic_10f.arff"],
#                    "Multilayer_Perceptron_RepCV" : ["GazeWrapper_Best1st_MultilayerPerc_10f.arff"],
#                    "Naive_Bayes_RepCV" : ["GazeWrapper_best1st_NaiveBayes_10f.arff"],
#                    #"Random_Forest_RepCV" : ["Gaze_Best1st_Random_12f.arff"],
#                    #"MultinomialLogisticRegression_RepCV" : ["Gaze_Best1st_Logistic_10percent.arff"]
#                    }
# final_acc, final_var, final_LL_acc, final_HL_acc, final_kappa, run_acc, run_LL_acc, run_HL_acc, run_kappa = runClassifierWithRepeatedCV(DIR, Classifiers_Gaze, NRUNS, NFOLDS, CVSamples, True)
# print final_acc, final_var, final_LL_acc, final_HL_acc, final_kappa, run_acc, run_LL_acc, run_HL_acc, run_kappa
# line = "Gaze" + "\t"  + "EnsembleGaze" + "\t"+str(final_acc) +"\t"+ str(final_var*100) +"\t" +str(final_LL_acc) + "\t" +str(final_HL_acc) + "\t" + str(final_kappa)+"\t"
# line = line + (str(run_acc)[1:-1]).replace(",", "\t") +"\t" + (str(run_kappa)[1:-1]).replace(",", "\t") + "\t" + (str(run_LL_acc)[1:-1]).replace(",", "\t") +  "\t" + (str(run_HL_acc)[1:-1]).replace(",", "\t") + "\n"
# lines.append(line)
#  
# print "Working on Full"
# 
# Classifiers_Full = {"Simple_Logistic_Regression_RepCV" : ["Backward_Full110_SimpleLogistic_10f.arff"],
#                    "Multilayer_Perceptron_RepCV" : ["Full110_MultilayerPerceptron_10.arff"],
#                    "Naive_Bayes_RepCV" : ["Full110_NaiveBayes_10arff.arff"],
#                    #"Random_Forest_RepCV" : ["Full110_RandomForest_10arff.arff"],
#                    #"MultinomialLogisticRegression_RepCV" : ["Full110_MultinomialLogistic_10.arff"]
#                    }
#  
# final_acc, final_var, final_LL_acc, final_HL_acc, final_kappa, run_acc, run_LL_acc, run_HL_acc, run_kappa = runClassifierWithRepeatedCV(DIR, Classifiers_Full, NRUNS, NFOLDS, CVSamples, True)
# print final_acc, final_var, final_LL_acc, final_HL_acc, final_kappa, run_acc, run_LL_acc, run_HL_acc, run_kappa
# line = "Full" + "\t"  + "EnsembleFull" + "\t"+str(final_acc) +"\t"+ str(final_var*100) +"\t" +str(final_LL_acc) + "\t" +str(final_HL_acc) + "\t" + str(final_kappa)+"\t"
# line = line + (str(run_acc)[1:-1]).replace(",", "\t") +"\t" + (str(run_kappa)[1:-1]).replace(",", "\t") + "\t" + (str(run_LL_acc)[1:-1]).replace(",", "\t") +  "\t" + (str(run_HL_acc)[1:-1]).replace(",", "\t") + "\n"
# lines.append(line)
# 
# Classifiers_3Best_1 =  {"Simple_Logistic_Regression_RepCV" : ["GazeWrapper_best1st_SimpleLogistic_10f.arff", "Backward_Full110_SimpleLogistic_10f.arff"],
#                    "Multilayer_Perceptron_RepCV" : ["Full110_MultilayerPerceptron_10.arff"]
#                    }
#  
# Classifiers_5Best_2 = {"Simple_Logistic_Regression_RepCV" : ["GazeWrapper_best1st_SimpleLogistic_10f.arff"],
#                    "Multilayer_Perceptron_RepCV" : ["Actions36_MultilayerPerceptron_10.arff", "Full110_MultilayerPerceptron_10.arff"],
#                    "Naive_Bayes_RepCV" : ["GazeWrapper_best1st_NaiveBayes_10f.arff", "Full110_NaiveBayes_10arff.arff"],
#                    }
# Classifiers_3Best_3 = {"Simple_Logistic_Regression_RepCV" : ["GazeWrapper_best1st_SimpleLogistic_10f.arff"],
#                    "Multilayer_Perceptron_RepCV" : ["Actions36_MultilayerPerceptron_10.arff"],
#                    "Naive_Bayes_RepCV" : ["Full110_NaiveBayes_10arff.arff"],
#                    }
#   
# final_acc, final_var, final_LL_acc, final_HL_acc, final_kappa, run_acc, run_LL_acc, run_HL_acc, run_kappa = runClassifierWithRepeatedCV(DIR, Classifiers_3Best_1, NRUNS, NFOLDS, CVSamples, True)
# print final_acc, final_var, final_LL_acc, final_HL_acc, final_kappa, run_acc, run_LL_acc, run_HL_acc, run_kappa
# line = "MISC" + "\t"  + "Ensemble_3Best_1" + "\t"+str(final_acc) +"\t"+ str(final_var*100) +"\t" +str(final_LL_acc) + "\t" +str(final_HL_acc) + "\t" + str(final_kappa)+"\t"
# line = line + (str(run_acc)[1:-1]).replace(",", "\t") +"\t" + (str(run_kappa)[1:-1]).replace(",", "\t") + "\t" + (str(run_LL_acc)[1:-1]).replace(",", "\t") +  "\t" + (str(run_HL_acc)[1:-1]).replace(",", "\t") + "\n"
# lines.append(line)
#  
# final_acc, final_var, final_LL_acc, final_HL_acc, final_kappa, run_acc, run_LL_acc, run_HL_acc, run_kappa = runClassifierWithRepeatedCV(DIR, Classifiers_5Best_2, NRUNS, NFOLDS, CVSamples, True)
# print final_acc, final_var, final_LL_acc, final_HL_acc, final_kappa, run_acc, run_LL_acc, run_HL_acc, run_kappa
# line = "MISC" + "\t"  + "Ensemble_3Best_1" + "\t"+str(final_acc) +"\t"+ str(final_var*100) +"\t" +str(final_LL_acc) + "\t" +str(final_HL_acc) + "\t" + str(final_kappa)+"\t"
# line = line + (str(run_acc)[1:-1]).replace(",", "\t") +"\t" + (str(run_kappa)[1:-1]).replace(",", "\t") + "\t" + (str(run_LL_acc)[1:-1]).replace(",", "\t") +  "\t" + (str(run_HL_acc)[1:-1]).replace(",", "\t") + "\n"
# lines.append(line)
#  
# final_acc, final_var, final_LL_acc, final_HL_acc, final_kappa, run_acc, run_LL_acc, run_HL_acc, run_kappa = runClassifierWithRepeatedCV(DIR, Classifiers_3Best_3, NRUNS, NFOLDS, CVSamples, True)
# print final_acc, final_var, final_LL_acc, final_HL_acc, final_kappa, run_acc, run_LL_acc, run_HL_acc, run_kappa
# line = "MISC" + "\t"  + "Ensemble_3Best_1" + "\t"+str(final_acc) +"\t"+ str(final_var*100) +"\t" +str(final_LL_acc) + "\t" +str(final_HL_acc) + "\t" + str(final_kappa)+"\t"
# line = line + (str(run_acc)[1:-1]).replace(",", "\t") +"\t" + (str(run_kappa)[1:-1]).replace(",", "\t") + "\t" + (str(run_LL_acc)[1:-1]).replace(",", "\t") +  "\t" + (str(run_HL_acc)[1:-1]).replace(",", "\t") + "\n"
# lines.append(line)
#  
#  
# input = OUTDIR + "EnsembleLast_best3_VERY_final.tsv"
# with open(input, 'w') as f:
#     for line in lines:
#         #print line
#         f.write(line)


log.close()