'''
Created on Aug 2, 2013

@author: bondaria
'''
DISTR = [[2, 2, 2, 2, 2, 3, 3, 3, 3, 3], [3, 3, 3, 3, 3, 2, 2, 2, 2, 2]]
CLASSES = 2
import random
from Ensemble_WekaTools import *
from arfftools import findStartOfData
from stattools import *
import scipy as sc
import re
def GenerateDictClasses(INPUT, CLASSLABELS = ["LL", "HL"]):
    """
    Creates dictionary with elements of CLASSLABELS as keys and 
    list of all elements from INPUT finle  with matching label
    
    Note:this function works only with 2-letter labels. Otherwise should be changed
    """
    print "Generating class elements lists"
    with open(INPUT, 'r') as f: #read file
        lines = f.readlines()
    curstr = findStartOfData(INPUT)
    dict_classes = {}
    for el in CLASSLABELS:
        dict_classes[el] = []
    i = 0
    while curstr < len(lines):
        line = lines[curstr]
        dict_classes[line[-3:-1]].append(i) # !!!!!! change this line if you have other labels
        i += 1
        curstr += 1
    return dict_classes 

def GenerateFolds_for_nruns(class_LL, class_HL, distr, N, K):
    """
    Generates random folds given 
    """
    rnd = random.Random()
    runfolds = []
    for run in xrange(N):
        rnd.seed(run+8)
        folds = []
        curLL = 0
        curHL = 0
        #print "Current Run is " + str(run)
        rnd.shuffle(class_LL)
        rnd.shuffle(class_HL)
        Class1_i = run % 2
        Class2_i = 1 - (run % 2)
        for numfold in xrange(K):
            curfold = []
            #print "Cur Distr is: " + str(distr[Class1_i][numfold]) + " + "+ str(distr[Class2_i][numfold])
            for j in xrange(distr[Class1_i][numfold]):
                curfold.append(class_LL[curLL])
                curLL += 1
            for j in xrange(distr[Class2_i][numfold]):
                curfold.append(class_HL[curHL])
                curHL += 1
            folds.append(curfold)
        #print "FOLDS:"
        #for fold in folds:
        #    print fold 
        #check of intersections:
        for i in xrange(K):
            for j in xrange(K):
                if j > i:
                    if [val for val in folds[i] if val in folds[j]] != []:
                        raise RuntimeError("Sampling process is incorrect. Check code!")
    #                 else:
    #                     print "All is OK. Proceed"
        runfolds.append(folds)
    return runfolds


def CreateSamplesStratified(INPUT, N = 10, K = 10):
    """
    This function creates folds for N-repeated K-fold Stratified Cross Validation 
    (Stratified - samples are created balanced in terms of class distribution)
    Input:
    N - number of runs in Repeated CV
    K - number of folds in Repeated CV
     
    """
    if (N ==10) and (K==10):
        dict_classes = GenerateDictClasses(INPUT, CLASSLABELS = ["LL", "HL"])
        return GenerateFolds_for_nruns(dict_classes["LL"], dict_classes["HL"], DISTR, N, K)
    else:
        raise RuntimeError('current implementation supports only 10-fold cross Validation. Sorry for inconvenience')
    return
def CreateTrainTest(DIR, INPUT, testfold, trainFileName, testFileName,DIRTRAIN=None):
    """
    Create Train and Test Files from given .arff file 
    DIR+INPUT = path to the input .arff file
    testfold - ids of elements in INPUT that should be added to the test file
    Everything not included to testfold will be written in trainfile
    DIRTRAIN - directory for writing Test and Train files. DIR is used if DIRTRAIN is not specified
    """
    
    #trainFileName = INPUT[:-5]+"-train" + suffix +".arff"
    #testFileName = INPUT[:-5]+"-test"+ suffix +".arff"
    if DIRTRAIN is not None:
        trainfile = open(DIRTRAIN + trainFileName,"w")
        testfile = open(DIRTRAIN + testFileName,"w")
    else: 
        """IF DIRTRAIN is not specified DIR is used instead"""
        #print "creating Test and Train"
        trainfile = open(DIR + trainFileName,"w")
        testfile = open(DIR + testFileName,"w")
    with open(DIR + INPUT, 'r') as f:
        lines = f.readlines()
        featstart = findStartOfData(DIR + INPUT)
        for curstr in xrange(featstart):
            trainfile.write(lines[curstr])
            testfile.write(lines[curstr])
        i = 0
        for curstr in range(featstart,len(lines)):
            if i in testfold:
                testfile.write(lines[curstr])
            else:
                trainfile.write(lines[curstr])
            i += 1
            curstr += 1
    testfile.close()
    trainfile.close()
    
def runClassifierWithRepeatedCV(DIR, dict_Classifiers, N, K, CVSamples, Ensemble = False, CombMethod = "Majority"):
    #def runClassifierWithRepeatedCV(DIR, INPUT, Classifier, N, K, Ensemble = False):
    """
    This function trains classifier and tests it with N run K fold CV
    Inputs:
    DIR - directory
    dict_Classifiers - dictionary with Algorithms as keys and lists of corresponding 
    """
    
    run_TLL = []
    run_FLL = []
    run_THL = []
    run_FHL = []
    run_acc = []
    run_LL_acc = []
    run_HL_acc = []
    run_kappa = []
    for currun in CVSamples: #for each run
        #for each run calculate aggregated stats. 
        TLL = 0
        FLL = 0
        THL = 0 
        FHL = 0
        for curfold in currun:
            if not Ensemble:
                curTLL, curFLL, curTHL, curFHL = AnyClassifier(DIR, dict_Classifiers, curfold)
            else:
                curTLL, curFLL, curTHL, curFHL = EnsembleClassifer(DIR, dict_Classifiers, curfold, CombMethod)
            
            TLL += curTLL
            FLL += curFLL
            THL += curTHL 
            FHL += curFHL
            
            #
                
        run_TLL.append(TLL)    
        run_FLL.append(FLL)
        run_THL.append(THL)
        run_FHL.append(FHL)
        acc_overall, acc_LL, acc_HL, kappa_overall  = processOneRun(TLL, FLL, THL, FHL)
        run_acc.append(acc_overall)
        run_LL_acc.append(acc_LL)
        run_HL_acc.append(acc_HL)
        run_kappa.append(kappa_overall)
#     print "TLL", run_TLL
#     print "FLL", run_FLL
#     print "THL", run_THL
#     print "FHL", run_FHL
#     print "Overall Accuracy: ", run_acc
#     print "Overall Accuracy: ", 1.0 * sum(run_acc) / len(run_acc)
#     print "LL Accuracy: ", run_LL_acc
#     print "LL Accuracy: ", 1.0 * sum(run_LL_acc) / len(run_LL_acc)
#     print "HL Accuracy: ", run_HL_acc
#     print "HL Accuracy: ", 1.0 * sum(run_HL_acc) / len(run_HL_acc)
#     print "Average Kappa: ", run_kappa
#     print "Average Kappa: ", 1.0 * sum(run_kappa) / len(run_kappa)
    final_acc = 1.0 * sum(run_acc) / len(run_acc)
    #calculate variance:
    MSE = 0
    for el in run_acc:
        MSE += (el - final_acc) ** 2
    final_var = sc.math.sqrt((float(MSE))/len(run_acc))
#    print "Variance: " + str(final_var*100)[:6]    
    final_LL_acc = 1.0 * sum(run_LL_acc) / len(run_LL_acc)
    final_HL_acc = 1.0 * sum(run_HL_acc) / len(run_HL_acc)
    final_kappa = 1.0 * sum(run_kappa) / len(run_kappa)
    
    return final_acc, final_var, final_LL_acc, final_HL_acc, final_kappa, run_acc, run_LL_acc, run_HL_acc, run_kappa
    

def AnyClassifier(DIR, dict_Classifiers, curfold):
   
    curTLL = -1
    curFLL = -1
    curTHL = -1 
    curFHL = -1
    resfiles = []
    Classifiers = dict_Classifiers.keys()
    for Classifier in Classifiers:
        for file in dict_Classifiers[Classifier]:
            trainFileName = "train_"+file[:-5]+".arff"
            testFileName = "test_"+file[:-5]+".arff"
            resultFileName = "result"+file[:-5]+".txt"
            resfiles.append(resultFileName)
            CreateTrainTest(DIR, file, curfold, trainFileName, testFileName)
            exec ("curTLL, curFLL, curTHL, curFHL = " + Classifier + "(DIR, trainFileName, testFileName, resultFileName)")
            #print curTLL, curFLL, curTHL, curFHL
            #the following are defined in the previous exec statement
            if (curTLL < 0) or (curFLL < 0) or  (curTHL < 0) or (curTLL < 0):
                raise NameError("Classifier didn't work properly") 
    
    return curTLL, curFLL, curTHL, curFHL
    
def EnsembleClassifer(DIR, dict_Classifiers, curfold, CombMethod = "Majority"):
    """
    this method builds ensemble classifier using models
    listed in Classifiers.
    DIR - current directory with related files
    dict_Classifiers has Classifier names as keys and corresponding input file names as values.  
    curfold - current fold (list of # that form test set. Everything else forms train set)
    """
    curTLL = -1
    curFLL = -1
    curTHL = -1 
    curFHL = -1
    resfiles = []
    Classifiers = dict_Classifiers.keys()
    for Classifier in Classifiers:
        for file in dict_Classifiers[Classifier]:
            trainFileName = "train_"+file[:-5]+".arff"
            testFileName = "test_"+file[:-5]+".arff"
            resultFileName = "result"+file[:-5]+".txt"
            resfiles.append(resultFileName)
            CreateTrainTest(DIR, file, curfold, trainFileName, testFileName,)
            exec ("curTLL, curFLL, curTHL, curFHL = " + Classifier + "(DIR, trainFileName, testFileName, resultFileName)")
            if (curTLL < 0) or (curFLL < 0) or  (curTHL < 0) or (curTLL < 0):
                raise NameError("Classifier didn't work properly")
            
    if CombMethod == "Distr":
        return Ensemble_Classifier_process_distr(DIR, resfiles)
    else:
        return Ensemble_Classifier_process_majority(DIR, resfiles)

def Ensemble_Classifier_process_majority(DIR, resfiles):
    """
    Processes output from EnsembleClassifier
    input:
    DIR - Directory with resfiles
    resfiles - list of .txt raw output from WEKA
    """
    dict_results = {}
    dictModelResult = {}
    NumberOfTestedUsers = 0
    for file in resfiles:
        NumberOfTestedUsers = 0
        #res contains pairs of values: [<actual>, <predicted>] 
        dictModelResult [file] = []
        res=[]
        with open(DIR + file, "r") as resfile: 
            lines = resfile.readlines()
        
        for line in lines:
            if line.count(":")==2:
                c = line.split(":")
                res.append([int(c[0][-1]),int(c[1][-1])])
                NumberOfTestedUsers += 1
                # [<actual>, <predicted>] 
                #print [int(c[0][-1]),int(c[1][-1])]
        dictModelResult[file] = res
        
    #print res
    #print dictModelResult
    
    resEnsemble = np.zeros([NumberOfTestedUsers, CLASSES]) #<2 lines correspond to two classes>
    actualValue = np.zeros([NumberOfTestedUsers])
    results = np.zeros([NumberOfTestedUsers])
    #print "Number of users = ", str(NumberOfTestedUsers)
    #calculate actual value
    for i in xrange(NumberOfTestedUsers):
        actualValue[i] = dictModelResult[dictModelResult.keys()[0]][i][0]
    for key in dictModelResult.keys():
        dict_results[key] = np.zeros([NumberOfTestedUsers])
    for i in xrange(NumberOfTestedUsers):
        #keyModelResult - unique value for each feature set
        for keyModelResult in dictModelResult.keys():
            #print "keyModelResult whatever it is: " + keyModelResult
            #check if actual values match between the models
            el = dictModelResult[keyModelResult][i]
            #print el
            if el[0] == actualValue[i]:
                #if matched
                #increment corresponding element of array
                #print i, el[1]
                resEnsemble[i, el[1]-1]+= 1
                dict_results[keyModelResult][i] = el[1]
            else: print "Error! Folds don't match!"
        #majority voting
        results[i] = np.argmax(resEnsemble[i])+1
        
    return CalulcateStatsFromLists(actualValue, results, NumberOfTestedUsers)



#Distribution summation in Majority voting

def Ensemble_Classifier_process_distr(DIR, resfiles):
    """
    Processes output from EnsembleClassifier
    input:
    DIR - Directory with resfiles
    resfiles - list of .txt raw output from WEKA
    """
    dict_results = {}
    dictModelResult = {}
    dictModelProb = {}
    NumberOfTestedUsers = 0
    for file in resfiles:
        NumberOfTestedUsers = 0
        #res contains pairs of values: [<actual>, <predicted>] 
        dictModelResult [file] = []
        dictModelProb [file] = []
        res=[]
        prob = []
        with open(DIR + file, "r") as resfile: 
            lines = resfile.readlines()
        
        
        for line in lines:
            if line.count(":")==2:
                cur = re.findall("[-+]?[0-9]*[.]?[0-9]+",line, 0)
                #print "line: ", line
                #print "result: ", cur
                res.append([int(cur[1]),int(cur[2])])
                prob.append([int(cur[2]), float(cur[3])]) # first element of the pair is predicted value, second - probability
                NumberOfTestedUsers += 1
                # [<actual>, <predicted>] 
                #print [int(c[0][-1]),int(c[1][-1])]
        dictModelResult[file] = res
        dictModelProb [file] = prob
        
    #print res
    #print dictModelResult
    
    resEnsemble = np.zeros([NumberOfTestedUsers, CLASSES]) #<2 lines correspond to two classes>
    actualValue = np.zeros([NumberOfTestedUsers])
    results = np.zeros([NumberOfTestedUsers])
    #print "Number of users = ", str(NumberOfTestedUsers)
    #calculate actual value
    for i in xrange(NumberOfTestedUsers):
        actualValue[i] = dictModelResult[dictModelResult.keys()[0]][i][0]
    for key in dictModelResult.keys():
        dict_results[key] = np.zeros([NumberOfTestedUsers])
    for i in xrange(NumberOfTestedUsers):
        #keyModelResult - unique value for each feature set
        for keyModelResult in dictModelResult.keys():
            #print "keyModelResult whatever it is: " + keyModelResult
            #check if actual values match between the models
            el = dictModelResult[keyModelResult][i]
            elprob = dictModelProb[keyModelResult][i]
            #print el
            if el[0] == actualValue[i]:
                #if matched
                #increment corresponding element of array
                #print i, el[1]
                resEnsemble[i, elprob[0]-1]+= elprob[1]
                resEnsemble[i, 2 - elprob[0]]+= 1 - elprob[1]
                dict_results[keyModelResult][i] = el[1]
            else: print "Error! Folds don't match!"
        #majority voting
        results[i] = np.argmax(resEnsemble[i])+1
        print "Ensemble modeling: ", resEnsemble
        print "Initial prob: ", dictModelProb[keyModelResult]
        print "Actual value", actualValue
    return CalulcateStatsFromLists(actualValue, results, NumberOfTestedUsers)