'''
Created on Aug 2, 2013

@author: bondaria
'''
def read_Arff_file(input):
    #read file
    f = open(input, 'r')
    lines = f.readlines()
    f.close
    print "Processing file " + input
    nstr = 0
    
    #read all attributes
    
    # features are located in dictionary
    # KEY: attribute name
    # VALUE:  [] - list of features from first participant to the last participant
    featdict = {}
    featheaders = []
    while lines[nstr]!="@data\n":
        if lines[nstr].find("@attribute") == 0 : # find attribute
            separator =lines[nstr][10] 
            linar = lines[nstr][:-1].split(separator)
            featdict[linar[1]] = []
            featheaders.append(linar[1])
        nstr+=1
    #pass all blank strings if any
    while len(lines[nstr]) < 8:
        nstr += 1
    #print lines[nstr]    
    # go through all lines with feature values 
    for line in lines[nstr:]:
        linar = line[:-1].split(",")
        #print len(linar)
        #print len(featheaders)
        #print featheaders
        for i in xrange(len(linar)):
            featdict[featheaders[i]].append(linar[i])
    print "smth"
    return featdict

def findStartOfData(input):
    """
    Finds start of feature matrix in .arff
    return string number (numerations starts from 0)
    """
    #read file
    f = open(input, 'r')
    lines = f.readlines()
    f.close
    nstr = 0
    while lines[nstr]!="@data\n":
        nstr+=1
    #pass all blank strings if any
    while len(lines[nstr]) < 8:
        nstr += 1
    return nstr  
    
#inputfile = "C:\\Users\\bondaria\\Documents\\UAI\\temp\\INPUT.arff"
#featdict = read_Arff_file(inputfile)

#print findStartOfData(inputfile)